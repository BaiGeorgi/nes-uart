#include <Servo.h> 
Servo myservo1;
Servo myservo2;
Servo camera;
int count=90;
void setup() {
  Serial.begin(9600);
  myservo1.detach();
  myservo2.detach();
  camera.attach(11);
  camera.write(count);
  delay(1000);
  camera.detach();
  Serial.println("Ready");
}

void loop() {
  if (Serial.available() > 0) {
    int inByte = Serial.read();
    switch (inByte) {
     case 'w'://Move forward
      myservo1.attach(9);
      myservo2.attach(10);
      myservo1.write(180);
      myservo2.write(0);
      delay(10);
     break;
     case 's'://Move backward
      myservo1.attach(9);
      myservo2.attach(10);
      myservo1.write(0);
      myservo2.write(180);
      delay(10);
     break;
     case 'a'://Rotate left
      myservo1.attach(9);
      myservo2.attach(10);
      myservo1.write(0);
      myservo2.write(0);
      delay(10);
     break;
     case 'd'://Rotate right
      myservo1.attach(9);
      myservo2.attach(10);
      myservo1.write(180);
      myservo2.write(180);
      delay(10);
     break;
     case '*'://Shake
      myservo1.attach(9);
      myservo2.attach(10);
      myservo1.write(180);
      myservo2.write(180);
      delay(100);
      myservo1.write(0);
      myservo2.write(0);
      delay(100);
      myservo1.write(180);
      myservo2.write(180);
      delay(100);
      myservo1.write(0);
      myservo2.write(0);
      delay(100);
      myservo1.write(180);
      myservo2.write(180);
      delay(100);
      myservo1.write(0);
      myservo2.write(0);
      delay(100);
      myservo1.write(180);
      myservo2.write(180);
      delay(100);
      myservo1.write(0);
      myservo2.write(0);
      delay(100);
      myservo1.detach();
      myservo2.detach();
     break;
     case '0'://Stop
      myservo1.detach();
      myservo2.detach();
     break;
     case 'z'://Stop
      myservo1.detach();
      myservo2.detach();
     break;
     case '+'://Adjust camera position up with 1°
      count++;
      Serial.println(count);
      camera.attach(11);
      camera.write(count);
     break;
     case '-'://Adjust camera position down with 1°
      count--;
      Serial.println(count);
      camera.attach(11);
      camera.write(count);
     break;
     case '5'://Adjust camera position down with 1°
      count=90;
      Serial.println(count);
      camera.attach(11);
      camera.write(count);
     break;
     case '4'://Slow turn left
      myservo1.attach(9);
      myservo2.attach(10);
      myservo1.write(85);
      myservo2.write(85);
      delay(10);
     break;
     case '6'://Slow turn right
      myservo1.attach(9);
      myservo2.attach(10);
      myservo1.write(95);
      myservo2.write(95);
      delay(10);
     break;
     case '8'://Slow forward
      myservo1.attach(9);
      myservo2.attach(10);
      myservo1.write(170);
      myservo2.write(10);
     delay(10);
     case '2'://Slow backward
      myservo1.attach(9);
      myservo2.attach(10);
      myservo1.write(70);
      myservo2.write(110);
      delay(10);
     break;
    } 
  }
}