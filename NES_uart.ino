int latch = 5; // set the latch pin
int clock = 6; // set the clock pin
int datin = 4;// set the data in pin
int breaks = 1;
byte controller_data = 0;

void controllerRead() {
controller_data = 0;
digitalWrite(latch,LOW);
digitalWrite(clock,LOW);
digitalWrite(latch,HIGH);
delayMicroseconds(2);
digitalWrite(latch,LOW);

controller_data = digitalRead(datin);
for (int i = 1; i <= 7; i ++) {
 digitalWrite(clock,HIGH);
 delayMicroseconds(2);
 controller_data = controller_data << 1;
 controller_data = controller_data + digitalRead(datin) ;
 delayMicroseconds(4);
 digitalWrite(clock,LOW);
}
}
void setup() {
  Serial1.begin(9600);
  pinMode(latch,OUTPUT);
  pinMode(clock,OUTPUT);
  pinMode(datin,INPUT);
  digitalWrite(latch,HIGH);
  digitalWrite(clock,HIGH);
}

void loop() {
controllerRead();
//Serial.println(controller_data, BIN);
if (controller_data==B11101111){ //START
  Serial1.print("5");
  //Serial.print("5");
  delay(500);
//}else if (controller_data==B11011111){ //SELECT
//  Serial1.print("/");
//  delay(500);
}else if (controller_data==B01111111){ //A
  Serial1.print("+");
}else if (controller_data==B10111111){ //B
  Serial1.print("-");
}else if (controller_data==B11110111){ //UP
  breaks = 1;
  Serial1.print("w");
}else if (controller_data==B11111011){ //DOWN
  breaks = 1;
  Serial1.print("s");
}else if (controller_data==B11111110){ //RIGHT
  breaks = 1;
  Serial1.print("d");
}else if (controller_data==B11111101){ //LEFT
  breaks = 1;
  Serial1.print("a");
}else if (controller_data==B11010111){ //Camera Down
  Serial1.print("-");
}else if (controller_data==B11011011){ //Camera UP
  Serial1.print("+");
}else if (controller_data==B11001111){ //Camera ZERO
  Serial1.print("5");
}else if (controller_data==B11011101){ //Slow turn left
  Serial1.print("4");
}else if (controller_data==B11011110){ //Slow turn right
  Serial1.print("6");
}else if (controller_data==B11110101){ //UP+left
  Serial1.print("7");
}else if (controller_data==B11110110){ //UP+right
  Serial1.print("9");
}else if (controller_data==B11110101){ //DOWN+left
  Serial1.print("1");
}else if (controller_data==B11110110){ //DOWN+right
  Serial1.print("3");
}else{
  if (breaks == 1){
  breaks = 0;
  Serial1.print("0");
  Serial.print("0");
  }
}
delay(50);
}